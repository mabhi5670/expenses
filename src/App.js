import { useState } from 'react';
import './App.css';
import NewExpenses from './components/NewExpenses/NewExpenses';
import Expenses from './components/Expenses/Expenses';

const Initial_Expenses = [
  {
    id: 'e1',
    title: 'Electricity',
    amount: 294.67,
    date: new Date(2021, 2, 28),
  },
  {
    id: 'e2',
    title: 'Car Insurance',
    amount: 294.67,
    date: new Date(2021, 7, 14),
  },
  {
    id: 'e3',
    title: 'Grocery',
    amount: 300.67,
    date: new Date(2021, 2, 5),
  },
  {
    id: 'e4',
    title: 'New Desk (wooden)',
    amount: 450.67,
    date: new Date(2021, 5, 12),
  },
];

const App = () => {
  const [expenses, setExpenses] = useState(Initial_Expenses);
  localStorage.setItem('expense', { Initial_Expenses });
  const addExpenseHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };
  return (
    <div className='App'>
      <h2>Expenses</h2>
      <NewExpenses onAddExpense={addExpenseHandler} />
      <Expenses expenses={expenses} />
    </div>
  );
};

export default App;
