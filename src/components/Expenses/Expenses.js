import React, { useState } from 'react';
import ExpenseItem from '../ExpenseItems/ExpenseItems';
import './Expenses.css';
import Card from '../Card/Card';
import ExpensesChart from '../Chart/ExpensesChart';
import ExpensesFilter from '../ExpenseFilter/ExpensesFilter';

const Expenses = ({ expenses }) => {
  const [filter, setFilter] = useState('');
  localStorage.getItem('expense');
  const filterChangeHandler = (selectedYear) => {
    setFilter(selectedYear);
  };
  console.log(expenses, '<<<<>>>>>');
  const filterExpense = expenses.filter((items) => {
    console.log(expenses, '>>>>', '<<<<<<<<<', items.date.getFullYear());
    return items.date.getFullYear().toString() === filter;
  });
  console.log(filterExpense);
  return (
    <div>
      <Card className='expenses'>
        <ExpensesFilter
          selected={filter}
          onChangeFilterHandler={filterChangeHandler}
        />
        <ExpensesChart expenses={filterExpense} />
        {filterExpense.map((items) => (
          <ExpenseItem
            key={items.id}
            title={items.title}
            amount={items.amount}
            date={items.date}
          />
        ))}

        {/* {expenses.map((expenses) => (
        <ExpenseItems key={expenses.id} {...expenses} />
      ))} */}
      </Card>
    </div>
  );
};

export default Expenses;
