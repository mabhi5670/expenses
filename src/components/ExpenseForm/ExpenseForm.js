import React, { useState } from 'react';
import './ExpenseForm.css';

const ExpenseForm = ({ onSaveExpenseData }) => {
  const [newTitle, setNewTitle] = useState('');
  const [newAmount, setNewAmount] = useState('');
  const [newDate, setNewDate] = useState('');
  //   const [userInput, setUserInput] = useState({
  //     newTitle: '',
  //     newAmount: '',
  //     newDate: '',
  //   });

  const titleHandler = (event) => {
    setNewTitle(event.target.value);
    // setUserInput({ ...userInput, newTitle: event.target.value });
    // setUserInput((prevState) => {
    //   return { ...prevState, enteredTitle: event.target.value };
    // });
  };

  const amountHandler = (event) => {
    setNewAmount(event.target.value);
    //setUserInput({ ...userInput, newAmount: event.target.value });
  };

  const dateHandler = (event) => {
    setNewDate(event.target.value);
    // setUserInput({ ...userInput, newDate: event.target.value });
  };

  const submitHandler = (event) => {
    event.preventDefault();

    const expenseData = {
      title: newTitle,
      amount: +newAmount,
      date: new Date(newDate),
    };

    onSaveExpenseData(expenseData);
    console.log(expenseData);
    setNewTitle('');
    setNewAmount('');
    setNewDate('');
    // localStorage.setItem('expenseData',expenseData)
  };
  return (
    <form onSubmit={submitHandler}>
      <div className='new-expense__controls'>
        <div>
          <label className='new-expense__control_label'>Title</label>
          <input
            className='new-expense__control_input'
            type='text'
            value={newTitle}
            onChange={titleHandler}
          />
        </div>

        <div>
          <label className='new-expense__control_label'>Amount</label>
          <input
            className='new-expense__control_input'
            type='number'
            min='0.01'
            step='0.01'
            value={newAmount}
            onChange={amountHandler}
          />
        </div>

        <div>
          <label className='new-expense__control_label'>Date</label>
          <input
            className='new-expense__control_input'
            type='date'
            min='2019-01-01'
            max='2024-12-31'
            value={newDate}
            onChange={dateHandler}
          />
        </div>
      </div>

      <div className='new-expense__actions'>
        <button type='submit'>ADD</button>
      </div>
    </form>
  );
};

export default ExpenseForm;
