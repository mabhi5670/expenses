import React from 'react';
import ExpenseDate from '../ExpenseDate/ExpenseDate';
import './ExpenseItems.css';
import Card from '../Card/Card';

const ExpenseItems = ({ date, amount, title }) => {
  console.log('expense value');

  return (
    <Card className='expense-item'>
      <ExpenseDate date={date} />
      <div className='expense-item__description'>
        <h2>{title}</h2>
        <div className='expense-item__price'>₹{amount}</div>
      </div>
      {/* <button onClick={onClickHandler}>Change Title</button> */}
    </Card>
  );
};

export default ExpenseItems;
